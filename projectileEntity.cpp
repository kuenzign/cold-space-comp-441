#include "projectileEntity.h"

//=============================================================================
// default constructor
//=============================================================================
projectileEntity::projectileEntity(): Entity(){
	spriteData.width = projectileNS::WIDTH;           // size of Projectile
	spriteData.height = projectileNS::HEIGHT;
	spriteData.x = projectileNS::X;                   // location on screen
	spriteData.y = projectileNS::Y;
	spriteData.rect.bottom = projectileNS::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = projectileNS::WIDTH;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	frameDelay = projectileNS::PROJECTILE_ANIMATION_DELAY;
	startFrame = projectileNS::PROJECTILE_START_FRAME;     // first frame of projectile animation
	endFrame     = projectileNS::PROJECTILE_END_FRAME;     // last frame of projectile animation
	currentFrame = startFrame;
	radius = projectileNS::WIDTH/2.0;
	collisionType = entityNS::CIRCLE;
	tempProjectile = nullptr;
	mass = 0.5f;
	lives = 3;
}

//=============================================================================
// Draw the Projectile
//=============================================================================
void projectileEntity::draw()
{
	// draw projectile using colorFilter
	Entity::draw(spriteData, colorFilter);
	if(isReal){
		tempProjectile->draw();
	}
}

//=============================================================================
// Initialize the Projectile.
// Post: returns true if successful, false if failed
//=============================================================================
bool projectileEntity::initialize(Game *gamePtr, int width, int height, int ncols,
								  TextureManager *textureM, COLOR_ARGB color, bool parent)
{
	colorFilter = color;
	isReal = parent;

	if(isReal){
		tempProjectile = new projectileEntity();
		tempProjectile->initialize(gamePtr, width, height, ncols, textureM, color, false);
		tempProjectile->setVisible(false);
		tempProjectile->setScale(PROJECTILE_IMAGE_SCALE);
		tempProjectile->setVelocity(VECTOR2(projectileNS::SPEED, 0)); // VECTOR2(X, Y)
	}

	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void projectileEntity::update(float frameTime)
{
	Entity::update(frameTime);

	spriteData.angle += frameTime * projectileNS::ROTATION_RATE;  // rotate the projectile

	if(isReal){
		spriteData.x += frameTime * velocity.x;         // move projectile along X 
		spriteData.y += frameTime * velocity.y;         // move projectile along Y

		float tempDiff=0;
		tempProjectile->setY(spriteData.y);

		if(spriteData.x <= 0){ //If projectile goes off left side
			if(spriteData.x + (projectileNS::WIDTH*spriteData.scale) > 0){ //Projectile is still on screen
				//spawn temp projectile
				tempProjectile->setVisible(true);
				tempDiff = GAME_WIDTH;
			}else{
				//despawn temp projectile
				tempProjectile->setVisible(false);
				spriteData.x+=GAME_WIDTH;
			}
		}

		if(spriteData.x + (projectileNS::WIDTH*spriteData.scale) >= GAME_WIDTH){ //If projectile goes off right side
			if(spriteData.x < GAME_WIDTH){ //Projectile is still on screen
				//spawn temp projectile
				tempProjectile->setVisible(true);
				tempDiff = -(int)GAME_WIDTH;
			}else{
				//despawn temp projectile
				tempProjectile->setVisible(false);
				spriteData.x-=GAME_WIDTH;
			}
		}

		if (spriteData.y > GAME_HEIGHT-projectileNS::HEIGHT * spriteData.scale)  // if hit bottom screen edge
		{
			if(lives > 0){
				lives--;
				spriteData.y = GAME_HEIGHT/2;  // position at bottom screen edge
				spriteData.x = GAME_WIDTH/2;
			}
			//velocity.y = -velocity.y;                   // reverse Y direction
		} else if (spriteData.y < GAME_HEIGHT/7)                    // else if hit top screen edge
		{
			spriteData.y = GAME_HEIGHT/7;                           // position at top screen edge
			velocity.y = -velocity.y;                   // reverse Y direction
			audio->playCue(LASER);
		}
		tempProjectile->setX(spriteData.x + tempDiff);
		tempProjectile->update(frameTime);
	}
}

//=============================================================================
// Returns a pointer to the decoy projectile
//=============================================================================
projectileEntity projectileEntity::getDecoy(){
	if(isReal){
		return *tempProjectile;
	}
	return *this;
}


//=============================================================================
// Returns the number of lives
//=============================================================================
int projectileEntity::getLives(){
	return lives;
}

//=============================================================================
// destructor
//=============================================================================
projectileEntity::~projectileEntity(){
	delete tempProjectile;
}

////////////////////////////////////////
//           Set functions            //
////////////////////////////////////////

// Set active.
void projectileEntity::setActive(bool a){
	Entity::setActive(a);
	if(isReal && !a){
		tempProjectile->setActive(a);
	}
}

// Set visible.
void projectileEntity::setVisible(bool v){
	Entity::setVisible(v);
	if(isReal && !v){
		tempProjectile->setVisible(v);
	}
}