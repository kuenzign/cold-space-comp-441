#ifndef _ASTEROIDENTITY_H
#define _ASTEROIDENTITY_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"

namespace asteroidNS
{
	const RECT  COLLISION_RECTANGLE = {-32, -32, 32, 32};
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
    const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    const int   TEXTURE_COLS = 1;           // texture has 1 columns
    const int   ASTEROID_START_FRAME = 0;      // asteroid starts at frame 0
    const int   ASTEROID_END_FRAME = 0;        // asteroid animation frames 0
    const float ASTEROID_ANIMATION_DELAY = 0.0f;    // time between frames
}

class asteroidEntity: public Entity
{
public:
	asteroidEntity(void);

	// inherited member functions
    virtual void draw(void);
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM, COLOR_ARGB color);
    virtual void update(float frameTime);

private:
	
};

#endif