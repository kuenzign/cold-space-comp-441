#ifndef _SHIP_H
#define _SHIP_H
#define WIN32_LEAN_AND_MEAN

#include "image.h"
#include "input.h"

class ship: public Image
{
public:
	ship(UCHAR, UCHAR, COLOR_ARGB);
	void act(Input*, float, D3DXVECTOR2);

private:
	UCHAR right;
	UCHAR left;
	COLOR_ARGB color;
	#define SHIP_IMAGE_SCALE 0.2f

};

#endif