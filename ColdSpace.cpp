

#include "ColdSpace.h"
#include <time.h>


//=============================================================================
// Constructor
//=============================================================================
ColdSpace::ColdSpace()
{
	dxFontSmall	= new TextDX();     // DirectX fonts
	dxFontMedium = new TextDX();
	dxFontLarge	= new TextDX();
	endLine1="";
	endLine2="";
}

//=============================================================================
// Destructor
//=============================================================================
ColdSpace::~ColdSpace()
{
	releaseAll();           // call onLostDevice() for every graphics item
	SAFE_DELETE(dxFontSmall);
	SAFE_DELETE(dxFontMedium);
	SAFE_DELETE(dxFontLarge);
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void ColdSpace::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	srand((unsigned int)time(NULL));

	if (!backgroundTexture.initialize(graphics, BACKGROUND_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Background texture initialization failed"));
	if (!background.initialize(graphics, 0,0,0, &backgroundTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init background"));
	background.setX(GAME_WIDTH/2 - (background.getWidth()*BACKGROUND_IMAGE_SCALE)/2);
	background.setY(GAME_HEIGHT/2 - (background.getHeight()*BACKGROUND_IMAGE_SCALE)/2);
	background.setScale(BACKGROUND_IMAGE_SCALE);

	if (!logoTexture.initialize(graphics, LOGO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Logo texture initialization failed"));
	if (!logo.initialize(graphics, 0,0,0, &logoTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init logo"));
	logo.setX(GAME_WIDTH/2 - (logo.getWidth()*LOGO_IMAGE_SCALE)/2);
	logo.setY(GAME_HEIGHT/14 - (logo.getHeight()*LOGO_IMAGE_SCALE)/2);
	logo.setScale(LOGO_IMAGE_SCALE);

	if (!projectileTexture.initialize(graphics, PROJECTILE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Projectile texture initialization failed"));
	if (!projectile1.initialize(this, projectileNS::WIDTH, projectileNS::HEIGHT, projectileNS::TEXTURE_COLS, &projectileTexture, PLAYER1_COLOR, true))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init projectile"));
	projectile1.setScale(PROJECTILE_IMAGE_SCALE);
	projectile1.setX(GAME_WIDTH/4);
	projectile1.setY(3*GAME_HEIGHT/4);
	projectile1.setVelocity(VECTOR2(-projectileNS::SPEED,-projectileNS::SPEED)); // VECTOR2(X, Y)

	if (!projectile2.initialize(this, projectileNS::WIDTH, projectileNS::HEIGHT, projectileNS::TEXTURE_COLS, &projectileTexture, PLAYER2_COLOR, true))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init projectile"));
	projectile2.setScale(PROJECTILE_IMAGE_SCALE);
	projectile2.setX(GAME_WIDTH - GAME_WIDTH/4);
	projectile2.setY(3*GAME_HEIGHT/4);
	projectile2.setVelocity(VECTOR2(-projectileNS::SPEED,-projectileNS::SPEED)); // VECTOR2(X, Y)

	if (!shipTexture.initialize(graphics, SHIP_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Ship texture initialization failed"));
	if (!ship1.initialize(this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &shipTexture, VK_RIGHT, VK_LEFT, PLAYER1_COLOR, true))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init ship"));
	ship1.setX(GAME_WIDTH/2 - (ship1.getWidth()*SHIP_IMAGE_SCALE)/2);
	ship1.setY(GAME_HEIGHT - (ship1.getHeight()*SHIP_IMAGE_SCALE*2));
	ship1.setScale(SHIP_IMAGE_SCALE);

	if (!ship2.initialize(this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &shipTexture, 0x44, 0x41, PLAYER2_COLOR, true))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init ship"));
	ship2.setX(GAME_WIDTH/2 - (ship2.getWidth()*SHIP_IMAGE_SCALE)/2);
	ship2.setY(GAME_HEIGHT - (ship2.getHeight()*SHIP_IMAGE_SCALE*2));
	ship2.setScale(SHIP_IMAGE_SCALE);

	if (!asteroidTexture.initialize(graphics, ASTEROID_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Asteroid texture initialization failed"));
	for (int i = 0; i < 15; i++){
		if (!asteroid1[i].initialize(this, asteroidNS::WIDTH, asteroidNS::HEIGHT, asteroidNS::TEXTURE_COLS, &asteroidTexture, PLAYER1_COLOR))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init asteroid"));
		asteroid1[i].setScale(ASTEROID_IMAGE_SCALE);

		if (!asteroid2[i].initialize(this, asteroidNS::WIDTH, asteroidNS::HEIGHT, asteroidNS::TEXTURE_COLS, &asteroidTexture, PLAYER2_COLOR))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init asteroid"));
		asteroid2[i].setScale(ASTEROID_IMAGE_SCALE);
	}

	float startX = (float)GAME_WIDTH/2 - ((float)asteroidNS::WIDTH*ASTEROID_IMAGE_SCALE*(float)ASTEROID_COLUMNS/2);
	int a1=0, a2=0;
	for(int x = 0; x < ASTEROID_COLUMNS; x++){
		for(int y = 0; y < ASTEROID_ROWS; y++){
			while(true){
				bool randbool = rand() & 1;
				if(randbool && a1<15){
					asteroid1[a1].setX(startX+x*(float)asteroid1[a1].getWidth()*asteroid1[a1].getScale());
					asteroid1[a1].setY(GAME_HEIGHT/6+y*(float)asteroid1[a1].getHeight()*asteroid1[a1].getScale());
					a1++;
					break;
				}
				if(!randbool && a2<15){
					asteroid2[a2].setX(startX+x*(float)asteroid2[a2].getWidth()*asteroid2[a2].getScale());
					asteroid2[a2].setY(GAME_HEIGHT/6+y*(float)asteroid2[a2].getHeight()*asteroid2[a2].getScale());
					a2++;
					break;
				}
			}
		}
	}

	if (!gaugeBackTexture.initialize(graphics, GAUGE_BACK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Gauge back texture initialization failed"));
	if (!gaugeBack.initialize(graphics, 0,0,0, &gaugeBackTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init gauge back"));
	gaugeBack.setX(GAME_WIDTH - (gaugeBack.getWidth()*GAUGE_IMAGE_SCALE));
	gaugeBack.setY(0);
	gaugeBack.setScale(GAUGE_IMAGE_SCALE);

	if (!gaugeNeedleTexture.initialize(graphics, GAUGE_NEEDLE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Gauge needle texture initialization failed"));
	if (!gaugeNeedle.initialize(graphics, 0,0,0, &gaugeNeedleTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init gauge needle"));
	gaugeNeedle.setX(GAME_WIDTH - (gaugeNeedle.getWidth()*GAUGE_IMAGE_SCALE));
	gaugeNeedle.setY(0);
	gaugeNeedle.setScale(GAUGE_IMAGE_SCALE);
	gaugeNeedle.setRadians(NEEDLE_MIN_ANGLE);

	float startX1 = 0;
	float startX2 = GAME_WIDTH-(projectileNS::WIDTH*PROJECTILE_IMAGE_SCALE*0.5f);
	for(int i = 0; i < 3; i++){
		if (!life1[i].initialize(this,projectileNS::WIDTH, projectileNS::HEIGHT, projectileNS::TEXTURE_COLS, &projectileTexture, PLAYER1_COLOR, true))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init projectile"));
		life1[i].setScale(PROJECTILE_IMAGE_SCALE*0.5f);
		life1[i].setX(startX1+(i*projectileNS::WIDTH*PROJECTILE_IMAGE_SCALE*0.5f));
		life1[i].setY(GAME_HEIGHT - projectile1.getHeight()*PROJECTILE_IMAGE_SCALE*0.5f);
		life1[i].setActive(false);
		life1[i].setVisible(true);

		if (!life2[i].initialize(this,projectileNS::WIDTH, projectileNS::HEIGHT, projectileNS::TEXTURE_COLS, &projectileTexture, PLAYER2_COLOR, true))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init projectile"));
		life2[i].setScale(PROJECTILE_IMAGE_SCALE*0.5f);
		life2[i].setX(startX2-(i*projectileNS::WIDTH*PROJECTILE_IMAGE_SCALE*0.5f));
		life2[i].setY(GAME_HEIGHT - projectile1.getHeight()*PROJECTILE_IMAGE_SCALE*0.5f);
		life2[i].setActive(false);
		life2[i].setVisible(true);
	}

	countdownTime = MAX_TIME;
	isGameOver=false;

	audio->playCue(MUSIC);

	// initialize DirectX fonts
	// 15 pixel high Arial
	if(dxFontSmall->initialize(graphics, 28, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	// 62 pixel high Arial
	if(dxFontMedium->initialize(graphics, 62, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	// 124 pixel high Arial
	if(dxFontLarge->initialize(graphics, 124, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	score1=0;
	score2=0;

	return;
}

//=============================================================================
// Update all game items
//=============================================================================
void ColdSpace::update()
{
	countdownTime -= frameTime;
	if(isGameOver){
		if(score1>score2){ //Player 1 wins
			endLine2="Soviet Union Wins";
		}else if(score2>score1){ //Player 2 wins
			endLine2="United States Wins";
		}else{ //Tie
			endLine2="There was a tie";
		}
	}else{
		gaugeNeedle.setRadians(NEEDLE_MIN_ANGLE-countdownTime*(NEEDLE_MIN_ANGLE-NEEDLE_MAX_ANGLE)/MAX_TIME);
		ship1.update(frameTime);
		ship2.update(frameTime);
		for (int i = 0; i < 15; i++){
			asteroid1[i].update(frameTime);
			asteroid2[i].update(frameTime);
		}
		projectile1.update(frameTime);
		projectile2.update(frameTime);
		gaugeBack.update(frameTime);
		gaugeNeedle.update(frameTime);

		if(countdownTime <= 0){ //Ran out of oxygen
			isGameOver=true;
			endLine1="You ran out of oxygen!";
			audio->stopCue(MUSIC);
		}else if(projectile1.getLives() < 1 && projectile2.getLives() < 1){ //Ran out of ammunition
			isGameOver=true;
			endLine1="You both ran out of ammunition!";
			audio->stopCue(MUSIC);
		}else if(score1>=15 || score2>=15){ //Someone destroys all of their asteroids
			isGameOver=true;
			endLine1="Congratulations!";
			audio->stopCue(MUSIC);
		}
	}
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void ColdSpace::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void ColdSpace::collisions()
{
	if(!isGameOver){
		VECTOR2 collisionVector;
		// if collision between projectile and ship
		//audio->playCue(NOISE);
		if(projectile1.collidesWith(ship1, collisionVector))
		{
			// bounce off ship
			projectile1.bounce(collisionVector, ship1);
			audio->playCue(LASER);                  // play laser sound
		}else if(projectile1.collidesWith(ship1.getDecoy(), collisionVector))
		{
			// bounce off ship
			projectile1.bounce(collisionVector, ship1.getDecoy());
			audio->playCue(LASER);                  // play laser sound
		}else if(projectile1.getDecoy().collidesWith(ship1, collisionVector))
		{
			// bounce off ship
			projectile1.bounce(collisionVector, ship1);
			audio->playCue(LASER);                  // play laser sound
		}else if(projectile1.getDecoy().collidesWith(ship1.getDecoy(), collisionVector))
		{
			// bounce off ship
			projectile1.bounce(collisionVector, ship1.getDecoy());
			audio->playCue(LASER);                  // play laser sound
		}

		for (int i = 0; i < 15; i++){
			if(projectile1.collidesWith(asteroid1[i], collisionVector)){
				projectile1.bounce(collisionVector, asteroid1[i]);
				asteroid1[i].setActive(false);
				asteroid1[i].setVisible(false);
				audio->playCue(EXPLOSION);                  // play explosion sound
			}
			if(projectile1.collidesWith(asteroid2[i], collisionVector)){
				projectile1.bounce(collisionVector, asteroid2[i]);
				audio->playCue(LASER);                  // play laser sound
			}

			if(projectile2.collidesWith(asteroid2[i], collisionVector)){
				projectile2.bounce(collisionVector, asteroid2[i]);
				asteroid2[i].setActive(false);
				asteroid2[i].setVisible(false);
				audio->playCue(EXPLOSION);                  // play explosion sound
			}
			if(projectile2.collidesWith(asteroid1[i], collisionVector)){
				projectile2.bounce(collisionVector, asteroid1[i]);
				audio->playCue(LASER);                  // play laser sound
			}
		}

		// if collision between projectile and ship
		if(projectile2.collidesWith(ship2, collisionVector))
		{
			// bounce off ship
			projectile2.bounce(collisionVector, ship2);
			audio->playCue(LASER);                  // play laser sound
		}else if(projectile2.collidesWith(ship2.getDecoy(), collisionVector))
		{
			// bounce off ship
			projectile2.bounce(collisionVector, ship2.getDecoy());
			audio->playCue(LASER);                  // play laser sound
		}else if(projectile2.getDecoy().collidesWith(ship2, collisionVector))
		{
			// bounce off ship
			projectile2.bounce(collisionVector, ship2);
			audio->playCue(LASER);                  // play laser sound
		}else if(projectile2.getDecoy().collidesWith(ship2.getDecoy(), collisionVector))
		{
			// bounce off ship
			projectile2.bounce(collisionVector, ship2.getDecoy());
			audio->playCue(LASER);                  // play laser sound
		}
	}
	score1=0;
	score2=0;
	for(int i = 0; i < 15; i++){
		if(!asteroid1[i].getActive()){
			score1++;
		}
		if(!asteroid2[i].getActive()){
			score2++;
		}
	}

	if (projectile1.getLives() <= 0){
		ship1.setVisible(false);
		ship1.setActive(false);
		projectile1.setVisible(false);
		projectile1.setActive(false);
	}
	if (projectile2.getLives() <= 0){
		ship2.setVisible(false);
		ship2.setActive(false);
		projectile2.setVisible(false);
		projectile2.setActive(false);
	}

	for(int i = 2; i >= projectile1.getLives(); i--){
		life1[i].setVisible(false);
	}
	for(int i = 2; i >= projectile2.getLives(); i--){
		life2[i].setVisible(false);
	}
}

//=============================================================================
// Render game items
//=============================================================================
void ColdSpace::render()
{
	graphics->spriteBegin();                // begin drawing sprites
	background.draw();
	for (int i = 0; i < 15; i++){
		asteroid1[i].draw();
		asteroid2[i].draw();
	}
	logo.draw();
	gaugeBack.draw();
	gaugeNeedle.draw();
	for(int i = 0; i < 3; i++){
		life1[i].draw();
		life2[i].draw();
	}
	if(isGameOver)							//Display Game Over screen
	{
		float startY=GAME_HEIGHT/6 + (3*asteroidNS::HEIGHT*ASTEROID_IMAGE_SCALE);
		float deltaY=(GAME_HEIGHT-startY)/3;
		RECT rect[3];
		for(int i = 0; i < 3; i++){
			rect[i].left=GAME_WIDTH/8;
			rect[i].top=(long)(startY + i*deltaY);
			rect[i].right=7*GAME_WIDTH/8;
			rect[i].bottom=(long)(startY+(i+1)*deltaY);
		}
		dxFontSmall->setFontColor(graphicsNS::WHITE);
		dxFontMedium->setFontColor(graphicsNS::WHITE);
		//dxFontLarge->setFontColor(graphicsNS::YELLOW);
		dxFontMedium->print(endLine1,rect[0],DT_CENTER);
		dxFontMedium->print(endLine2,rect[1],DT_CENTER);
		dxFontSmall->print("Soviet Union: "+std::to_string(score1)+" asteroids",rect[2],DT_LEFT);
		dxFontSmall->print("United States: "+std::to_string(score2)+" asteroids",rect[2],DT_RIGHT);
	}
	else
	{
		ship1.draw();
		ship2.draw();
		projectile1.draw();
		projectile2.draw();
	}
	graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void ColdSpace::releaseAll()
{
	dxFontSmall->onLostDevice();
	dxFontMedium->onLostDevice();
	dxFontLarge->onLostDevice();
	logoTexture.onLostDevice();
	backgroundTexture.onLostDevice();
	shipTexture.onLostDevice();
	projectileTexture.onLostDevice();
	asteroidTexture.onLostDevice();
	gaugeBackTexture.onLostDevice();
	gaugeNeedleTexture.onLostDevice();
	logoTexture.onLostDevice();

	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void ColdSpace::resetAll()
{
	dxFontSmall->onResetDevice();
	dxFontMedium->onResetDevice();
	dxFontLarge->onResetDevice();
	logoTexture.onResetDevice();
	backgroundTexture.onResetDevice();
	shipTexture.onResetDevice();
	projectileTexture.onResetDevice();
	asteroidTexture.onResetDevice();
	gaugeBackTexture.onResetDevice();
	gaugeNeedleTexture.onResetDevice();

	Game::resetAll();
	return;
}