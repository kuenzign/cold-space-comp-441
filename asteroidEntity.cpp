#include "asteroidEntity.h"

//=============================================================================
// default constructor
//=============================================================================
asteroidEntity::asteroidEntity(): Entity(){
	spriteData.width = asteroidNS::WIDTH;           // size of Asteroid
    spriteData.height = asteroidNS::HEIGHT;
    spriteData.x = asteroidNS::X;                   // location on screen
    spriteData.y = asteroidNS::Y;
    spriteData.rect.bottom = asteroidNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = asteroidNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = asteroidNS::ASTEROID_ANIMATION_DELAY;
    startFrame = asteroidNS::ASTEROID_START_FRAME;     // first frame of asteroid animation
    endFrame     = asteroidNS::ASTEROID_END_FRAME;     // last frame of asteroid animation
    currentFrame = startFrame;
    radius = asteroidNS::WIDTH/2.0;
	collisionType = entityNS::CIRCLE;
	mass = 1000000.0f;
}

//=============================================================================
// Draw the Asteroid
//=============================================================================
void asteroidEntity::draw()
{
	// draw asteroid using colorFilter 50% alpha
	Entity::draw(spriteData, colorFilter);
}

//=============================================================================
// Initialize the Projectile.
// Post: returns true if successful, false if failed
//=============================================================================
bool asteroidEntity::initialize(Game *gamePtr, int width, int height, int ncols,
								  TextureManager *textureM, COLOR_ARGB color)
{
	colorFilter = color;
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}


//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void asteroidEntity::update(float frameTime)
{
	Entity::update(frameTime);
}