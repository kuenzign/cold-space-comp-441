#ifndef _SHIPENTITY_H
#define _SHIPENTITY_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "projectileEntity.h"

namespace shipNS
{
	const RECT  COLLISION_RECTANGLE = {-560, -84, 560, 84};
	const int WIDTH = 1120;                   // image width
	const int HEIGHT = 168;                  // image height
	const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
	const int Y = GAME_HEIGHT/2 - HEIGHT/2;
	const float SPEED = 200;                // 200 pixels per second
	const int   TEXTURE_COLS = 1;           // texture has 8 columns
	const int   SHIP_START_FRAME = 0;      // ship starts at frame 0
	const int   SHIP_END_FRAME = 19;        // ship ends at frame 19
	const float SHIP_ANIMATION_DELAY = 0.05f;    // time between frames
}

class shipEntity : public Entity
{
public:
	shipEntity(void);

	// inherited member functions
	virtual void draw(void);
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM, UCHAR right, UCHAR left,
		COLOR_ARGB color, bool parent);
	virtual void update(float frameTime);
	virtual void setActive(bool a);
	virtual void setVisible(bool v);

	shipEntity getDecoy(void);

	~shipEntity(void);

private:
	UCHAR rightKey;
	UCHAR leftKey;
	bool isReal;
	shipEntity *tempShip;
};

#endif