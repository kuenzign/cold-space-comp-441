
#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <d3d9.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }

#define VK_GREEN_RIGHT          0x23
#define VK_GREEN_LEFT			0x1C
//-----------------------------------------------
//                  Constants
//-----------------------------------------------
// graphic images
const char BACKGROUND_IMAGE[] = "pictures\\background.png";
const char ASTEROID_IMAGE[] = "pictures\\asteroid.png";
const char PROJECTILE_IMAGE[] = "pictures\\projectile.png";
const char SHIP_IMAGE[] = "pictures\\ship_anim.png";
const char LOGO_IMAGE[] = "pictures\\logo.png";
const char GAUGE_BACK_IMAGE[] = "pictures\\gauge_back.png";
const char GAUGE_NEEDLE_IMAGE[] = "pictures\\gauge_needle.png";

const float ASTEROID_IMAGE_SCALE = 1.0f;
const float PROJECTILE_IMAGE_SCALE = 0.2f;
const float SHIP_IMAGE_SCALE = 0.2f;
const float BACKGROUND_IMAGE_SCALE = 0.9f;
const float LOGO_IMAGE_SCALE = 1.0f;
const float GAUGE_IMAGE_SCALE = 1.0f;

// window
const char CLASS_NAME[] = "Cold Space";
const char GAME_TITLE[] = "Cold Space";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1080;               // width of game in pixels
const UINT GAME_HEIGHT = 640;               // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE  = 200.0f;               // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;   // escape key
const UCHAR ALT_KEY      = VK_MENU;     // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;   // Enter key

// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
const char WAVE_BANK[]  = "audio\\Win\\Wave Bank.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\Sound Bank.xsb";

// audio cues
const char LASER[] = "laser";
const char EXPLOSION[] = "explosion";
const char MUSIC[]= "music";

// guage
const float MAX_TIME = (float)(2*60);
const float NEEDLE_MAX_ANGLE = (float)PI/40;
const float NEEDLE_MIN_ANGLE = 39*(float)PI/40;

// asteroids
const int ASTEROID_ROWS = 3;
const int ASTEROID_COLUMNS = 10;

// colors
const DWORD PLAYER1_COLOR  = D3DCOLOR_ARGB(255, 220,  78,  58);
const DWORD PLAYER2_COLOR  = D3DCOLOR_ARGB(255,   0,  70, 140);

#endif