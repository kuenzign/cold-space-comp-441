#include "shipEntity.h"

//=============================================================================
// default constructor
//=============================================================================
shipEntity::shipEntity(): Entity(){
	spriteData.width = shipNS::WIDTH;           // size of Ship
	spriteData.height = shipNS::HEIGHT;
	spriteData.x = shipNS::X;                   // location on screen
	spriteData.y = shipNS::Y;
	spriteData.rect.bottom = shipNS::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = shipNS::WIDTH;
	velocity.x = 1;                             // velocity X
	velocity.y = 1;                             // velocity Y
	frameDelay = shipNS::SHIP_ANIMATION_DELAY;
	startFrame = shipNS::SHIP_START_FRAME;     // first frame of ship animation
	endFrame     = shipNS::SHIP_END_FRAME;     // last frame of ship animation
	currentFrame = startFrame;
	collisionType = entityNS::BOX;
	edge.left = -shipNS::WIDTH/2;
	edge.right = shipNS::WIDTH/2;
	edge.top = -shipNS::HEIGHT/2;
	edge.bottom = shipNS::HEIGHT/2;
	tempShip = nullptr;
	mass = 1000000.0f;
}

//=============================================================================
// draw the ship
//=============================================================================
void shipEntity::draw()
{
	// draw ship using colorFilter
	Entity::draw(spriteData, colorFilter);
	if(isReal){
		tempShip->draw();
	}
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool shipEntity::initialize(Game *gamePtr, int width, int height, int ncols,
							TextureManager *textureM, UCHAR right, UCHAR left,
							COLOR_ARGB color, bool parent)
{
	rightKey = right;
	leftKey = left;
	colorFilter = color;
	isReal = parent;

	if(isReal){
		tempShip = new shipEntity();
		tempShip->initialize(gamePtr, width, height, ncols, textureM, right, left, color, false);
		tempShip->setVisible(false);
		tempShip->setActive(false);
		tempShip->setScale(SHIP_IMAGE_SCALE);
	}

	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void shipEntity::update(float frameTime)
{
	Entity::update(frameTime);

	if(isReal){
		float tempDiff=0;
		tempShip->setY(spriteData.y);
		int directionX = 0;
		if(input->isKeyDown(rightKey)){
			directionX = 1;
		}
		if(input->isKeyDown(leftKey)){
			directionX = -1;
		}

		if(spriteData.x <= 0){ //If ship goes off left side
			if(spriteData.x + (shipNS::WIDTH*spriteData.scale) > 0){ //Ship is still on screen
				//spawn temp ship
				tempShip->setVisible(true);
				tempShip->setActive(true);
				tempDiff = GAME_WIDTH;
			}else{
				//despawn temp ship
				tempShip->setVisible(false);
				tempShip->setActive(false);
				spriteData.x+=GAME_WIDTH;
			}
		}

		if(spriteData.x + (shipNS::WIDTH*spriteData.scale) >= GAME_WIDTH){ //If ship goes off right side
			if(spriteData.x < GAME_WIDTH){ //Ship is still on screen
				//spawn temp ship
				tempShip->setVisible(true);
				tempShip->setActive(true);
				tempDiff = -(int)GAME_WIDTH;
			}else{
				//despawn temp ship
				tempShip->setVisible(false);
				tempShip->setActive(false);
				spriteData.x-=GAME_WIDTH;
			}
		}

		spriteData.x += directionX * frameTime * shipNS::SPEED;         // move ship along X
		tempShip->setX(spriteData.x + tempDiff);
		tempShip->update(frameTime);
	}
}

//=============================================================================
// Returns a pointer to the decoy ship
//=============================================================================
shipEntity shipEntity::getDecoy(){
	if(isReal){
		return *tempShip;
	}
	return *this;
}

//=============================================================================
// destructor
//=============================================================================
shipEntity::~shipEntity(){
	delete tempShip;
}

////////////////////////////////////////
//           Set functions            //
////////////////////////////////////////

// Set active.
void shipEntity::setActive(bool a){
	Entity::setActive(a);
	if(isReal && !a){
		tempShip->setActive(a);
	}
}

// Set visible.
void shipEntity::setVisible(bool v){
	Entity::setVisible(v);
	if(isReal && !v){
		tempShip->setVisible(v);
	}
}