

#ifndef _COLDSPACE_H             // Prevent multiple definitions if this 
#define _COLDSPACE_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "asteroidEntity.h"
#include "projectileEntity.h"
#include "shipEntity.h"
#include "graphics.h"
#include "textDX.h"
#include <string>

//=============================================================================
// This class is the core of the game
//=============================================================================
class ColdSpace : public Game
{
private:
    // game items
	TextureManager asteroidTexture;
	TextureManager shipTexture;
	TextureManager projectileTexture;
	TextureManager backgroundTexture;
	TextureManager logoTexture;
	TextureManager gaugeBackTexture;
	TextureManager gaugeNeedleTexture;
	TextureManager gameOverTexture;

	asteroidEntity asteroid1[15];
	asteroidEntity asteroid2[15];
	shipEntity ship1;
	shipEntity ship2;
	projectileEntity projectile1;
	projectileEntity projectile2;
	Image background;
	Image logo;
	Image gaugeBack;
	Image gameOver;
	projectileEntity life1[3];
	projectileEntity life2[3];
	Image gaugeNeedle;

	TextDX  *dxFontSmall;       // DirectX fonts
    TextDX  *dxFontMedium;
    TextDX  *dxFontLarge;

	float countdownTime;
	bool isGameOver;

	std::string endLine1, endLine2;
	int score1, score2;

public:
    // Constructor
    ColdSpace(void);

    // Destructor
    virtual ~ColdSpace(void);

    // Initialize the game
    void initialize(HWND hwnd);
    void update(void);      // must override pure virtual from Game
    void ai(void);          // "
    void collisions(void);  // "
    void render(void);      // "
    void releaseAll(void);
    void resetAll(void);
};

#endif
