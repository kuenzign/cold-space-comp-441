#ifndef _PROJECTILEENTITY_H
#define _PROJECTILEENTITY_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "input.h"

namespace projectileNS
{
    const int WIDTH = 240;                   // image width
    const int HEIGHT = 240;                  // image height
    const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    const float ROTATION_RATE = (float)PI; // radians per second
    const float SPEED = 150;                // 150 pixels per second
	const int   TEXTURE_COLS = 1;           // texture has 1 columns
    const int   PROJECTILE_START_FRAME = 0;      // projectile starts at frame 0
    const int   PROJECTILE_END_FRAME = 0;        // projectile animation frames 0
    const float PROJECTILE_ANIMATION_DELAY = 0.2f;    // time between frames
}

class projectileEntity: public Entity
{
public:
	projectileEntity(void);

	// inherited member functions
    virtual void draw(void);
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM, COLOR_ARGB color, bool parent);
    virtual void update(float frameTime);
	virtual void setActive(bool a);
	virtual void setVisible(bool v);

	projectileEntity getDecoy(void);
	int getLives(void);

	~projectileEntity(void);

private:
	bool isReal;
	projectileEntity *tempProjectile;
	int lives;
};

#endif